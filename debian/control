Source: pysoundfile
Section: python
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
 Alessio Treglia <alessio@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-buildinfo,
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 libsndfile1,
 python3,
 python3-cffi,
 python3-numpy,
 python3-pytest,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Testsuite: autopkgtest-pkg-python
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/bastibe/python-soundfile
Vcs-Git: https://salsa.debian.org/multimedia-team/pysoundfile.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/pysoundfile

Package: python3-soundfile
Architecture: all
Depends:
 libsndfile1,
 python3-cffi,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 python3-numpy,
 ${misc:Recommends},
 ${python3:Recommends},
 ${shlibs:Recommends},
Description: Python 3 audio module based on libsndfile
 'soundfile' can read and write sound files in a variety of formats and eases
 the application of signal processing algorithms.
 File reading/writing is supported through libsndfile.
 By default, 'soundfile' represents audio data as NumPy arrays, but ordinary
 Python buffers are supported as well.
 .
 This package provides the Python 3.x module.

Package: python-soundfile-doc
Section: doc
Architecture: all
Depends:
 libjs-mathjax,
 ${misc:Depends},
 ${sphinxdoc:Depends},
Built-Using:
 ${sphinxdoc:Built-Using},
Multi-Arch: foreign
Description: Python audio module based on libsndfile - documentation
 'soundfile' can read and write sound files in a variety of formats and eases
 the application of signal processing algorithms.
 File reading/writing is supported through libsndfile.
 By default, 'soundfile' represents audio data as NumPy arrays, but ordinary
 Python buffers are supported as well.
 .
 This package provides the API documentation.
